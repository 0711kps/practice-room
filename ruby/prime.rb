def getPrime(max)
  return 0 unless max >= 2

  list = (3..max).step(2).to_a
  delete_multiple = lambda do |x|
    list.delete_if { |el| (el % x).zero? && el != x }
  end
  [2] + list.tap { (3..Math.sqrt(max).to_i).each(&delete_multiple) }
end

puts getPrime(ARGV[0].to_i)
