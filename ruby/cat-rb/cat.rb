input_files = ARGV

input_files.each do |file|
  File.open(file, 'r').each_with_index do |line, index|
    puts "#{index + 1}| #{line}"
  end
  puts ""
end
