regexp = Regexp.new ARGV[0]
Dir.glob('**/*').filter { |v| v =~ regexp }.each do |v|
  puts v.sub(regexp, "\e[1;31m#{v.match(regexp)[0]}\e[0m")
end
