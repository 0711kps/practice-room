use std::io;
use std::io::prelude::*;
use rand::Rng;
use std::collections::HashSet;
use std::collections::HashMap;

fn main() {
    let cpu_secret_vec: Vec<u32> = gen_cpu_secret();
    round_process(cpu_secret_vec);
}

fn round_process(cpu_secret: Vec<u32>) {
    let mut game_set: bool = false;
    let cpu_secret_string: String = cpu_secret.iter().map(|c| c.to_string()).collect::<String>();
    while !game_set {
        let mut human_guess: String = String::new();
        println!("請輸入你猜測的電腦人的答案！");
        print!("-> ");
        io::stdout().flush()
            .expect("刷新失敗？");
        loop {
            io::stdin().read_line(&mut human_guess)
                .expect("讀取人類的輸入失敗！");
            if valify_uniq(&human_guess) {
                break;
            } else {
                println!("你的猜測有重複的數字！");
                print!("-> ");
                io::stdout().flush()
                    .expect("刷新失敗？");
                human_guess.clear();
            }
        }
        let ab_hash = calc_ab(&cpu_secret, &human_guess);
        println!("你猜的 {}, 結果是{}A{}B", human_guess, ab_hash.get(&'A').unwrap(), ab_hash.get(&'B').unwrap());
        if *ab_hash.get(&'A').unwrap() == 4 {
            println!("你贏了！就是 {}", cpu_secret_string);
            game_set = !game_set;
        }
    }
}

fn calc_ab(secret: &Vec<u32>, guess: &String) -> HashMap<&'static char, u8> {
    let mut ab_hash: HashMap<&'static char, u8> = HashMap::new();
    ab_hash.insert(&'A', 0);
    ab_hash.insert(&'B', 0);
    let guess: Vec<u32> = guess
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect();
    for (i, n) in guess.iter().enumerate() {
        if secret.contains(&n) {
            if secret[i] == guess[i] {
                *ab_hash.get_mut(&'A').unwrap() += 1;
            } else {
                *ab_hash.get_mut(&'B').unwrap() += 1;
            }
        }
    }
    ab_hash
}

fn gen_cpu_secret() -> Vec<u32> {
    let mut number_pool: Vec<u32> = Vec::new();
    for i in 0..10 {
        number_pool.push(i);
    }
    let mut cpu_secret: Vec<u32> = Vec::new();
    for _ in 0..4 {
        let number_len: u32 = number_pool.len() as u32;
        let i: u32 = rand::thread_rng().gen_range(0, number_len);
        cpu_secret.push(number_pool[rand::thread_rng().gen_range(0, number_pool.len())]);
        number_pool.remove(i as usize);
    }
    cpu_secret
}

fn valify_uniq(secret: &String) -> bool {
    let secret_set: HashSet<u32> = secret
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect();
    secret_set.len() == 4
}
