use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
  println!("猜個數字！");
  let secret_number = rand::thread_rng().gen_range(1, 101);
  loop {
      println!("你猜多少？");

      let mut guess = String::new();
      io::stdin().read_line(&mut guess)
        .expect("出錯啦！讀不到數字啊！！");
      let guess: u32 = match guess.trim().parse() {
          Ok(num) => num,
          Err(_) => continue,
      };
      println!("你剛剛猜了: {}", guess);

      match guess.cmp(&secret_number) {
          Ordering::Less => println!("太小啦！"),
          Ordering::Greater => println!("太大啦！"),
          Ordering::Equal => {
            println!("對對對，猜的剛剛好");
            break;
          }
      }
  }
}
